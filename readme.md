## All pages are linked to each other.

### For Example: 
1. If you click on "About Us" on the Home page, you will be redirected to the About Us page. And if you click on the logo-text(Heading of the page), you will be redirected to the Home page.

2. If you click on "Donate Now" on the Home page, you will be redirected to the Donation page. And if you click on the logo-text(Heading of the page) or go back link, you will be redirected to the Home page